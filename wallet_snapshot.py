import json
import pandas as pd
from etherscan_utils import *

if __name__ == '__main__':

    # Step1. Read Configs
    wallet_address = ""
    api_key = ""

    with open("./wallet_config.json") as file:
        wallet_config_map = json.load(file)
        wallet_address = wallet_config_map['wallet_address']
        api_key = wallet_config_map['api_key']

    coingecko = pd.read_json('./coingecko.json')

    with open("./token_address.json") as file:
        token_dict = json.load(file)

    # Step2. Main Logic

    temp = pd.DataFrame.from_dict(token_dict, orient='index', columns=['address', 'precision']).reset_index().rename(columns={'index': 'symbol'})
    temp['price'] = temp['symbol'].apply(lambda x: float('%f' % (get_coingecko_price(coingecko, x.lower()))))
    temp['quantity'] = temp['address'].apply(lambda x: get_quantity(api_key, wallet_address, x))
    temp['quantity'] = temp['quantity'] / temp['precision']
    temp['Value in USD'] = temp['price'] * temp['quantity']
    temp =  temp.where(pd.notnull(temp), None)

    print(temp)