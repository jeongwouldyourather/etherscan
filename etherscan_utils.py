import requests
import numpy as np

def get_coingecko_id(df, symbol):
    if symbol in df['symbol'].unique():
        return df[df['symbol'] == symbol]['id'].iloc[0]
    else:
        return False

def get_coingecko_price(coingecko_df, symbol):
    coingecko_id = get_coingecko_id(coingecko_df, symbol)
    if coingecko_id:
        return (requests.get(f'https://api.coingecko.com/api/v3/coins/{coingecko_id}/ohlc?vs_currency=usd&days=1')).json()[-1][-1]
    else:
        return np.nan

def get_quantity(api_key, wallet_address, address):
    if address == None:
        return int(requests.get(f"https://api.etherscan.io/api?module=account&action=balance&address={wallet_address}&tag=latest&apikey={api_key}").json()['result'])
    else:
        return int((requests.get(f"https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress={address}&address={wallet_address}&tag=latest&apikey={api_key}")).json()['result'])